require 'rails_helper'

describe ApplicationController do
  it "adds 1 and 1" do
    expect(1+1).to eq 2
  end
  
  it "adds 2 and 2" do
    expect(2+2).to eq 4
  end
end